import os 


for file in os.listdir("."):
    if ".png" in file:
        filename = file.split(".")[0]
        if "{}.bmp".format(filename) in os.listdir("."):
            os.remove("{}.bmp".format(filename))
        os.system("ln {}.png {}.bmp".format(filename, filename))