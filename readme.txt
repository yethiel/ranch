The Ranch

We started working on this track when R6 and URV visited me (Marv) in May 2016.
Our plan was to create a track while the guys were over at my house so we 
started drawing a plan on a large sheet of paper.
We started modeling immediately and shortly after, the base mesh was more or
less done. Some additions like the canyon made it into the track a year later.

We started adding more and more details to the track and came up with more
ideas as time went on. In late 2017 we found ourselves in a situation where we
had a track with a great concept and track flow. Still, there were so many 
things that had to be worked on. We took it too far. An open track like this is 
not an easy project. Despite all the difficulties and lack of motivation, we 
pushed through it and here we are now.

I developed a Blender add-on along the way which helped me to work out all the
details and the lighting. Managing all the instance files, applying properties
to all objects and making them look right would not have been possible
otherwise. You can find the source code on my GitHub profile: yethiel.

Material Notes:

Dirt 1:
	Most common surface type of the track.

Dirt 2:
	Used in the riverbed

Wood:
	Used for wooden planks (saloon, ranch house)

Wood 2:
	Used for the really rough planks in the barn and mill.
